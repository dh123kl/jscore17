$(document).ready(function() {
    let puzzles = []
    let leftIndex = 0
    let topIndex = 0
    for(let i = 0; i < 16; i++) {
        let puzzle = document.createElement('div')
        puzzle.classList.add('puzzle')
        puzzle.textContent = `${i}`
        puzzle.style.backgroundPosition = `${-leftIndex * 100}px ${-topIndex * 100}px`
        puzzles.push(puzzle)
        leftIndex++
        if(leftIndex == 4) {
            leftIndex = 0
            topIndex++
        }
    }

    let shuffle = (array) => {return array.sort(() => Math.random() - 0.5)}

    let timerInterval
    let mm
    let ss
    let timerStart = () => {
        timerTime = new Date().getTime() + 60000;
        timerInterval = setInterval(() => {
            let currentTime = new Date();
            diff = timerTime - currentTime;
            mm = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            ss = Math.floor((diff % (1000 * 60)) / 1000);
            if (ss < 10) ss = '0' + ss;
            $('.timer').text(`${mm}:${ss}`)
            if ($('.timer').text() == '0:00') {
                clearInterval(timerInterval);
                $('.modal-body').html(`<h3>Its a pity, but you lost</h3>`)
                $('#exampleModal').modal('show')
                $("#start-game").prop("disabled", true);
                $("#check-result").prop("disabled", true);
            }
        })
    }

    let getOffset = (element) => {
        let elementPos = element.offset();
        let containerPos = $('#end').offset();
        let elementOffset = {
            top: elementPos.top - containerPos.top,
            left: elementPos.left - containerPos.left
        }
        return elementOffset
    }

    let puzzlesOrder
    let timerStarted
    $('#new-game').on('click', function() {
        $('#start').html('')
        $('#end').html('')
        puzzlesOrder = []
        clearInterval(timerInterval);
        $('.timer').text('1:00')
        timerStarted = false

        puzzles = shuffle(puzzles)
        for(let i = 0; i < 16; i++) {
            $('#start').append(puzzles[i])
        }
        $('.puzzle').css('position', 'relative')
        $('.puzzle').css('left', '0')
        $('.puzzle').css('top', '0')
        $( "#start-game" ).prop( "disabled", false )
        $( "#check-result" ).prop( "disabled", true )
        
        $('.puzzle').draggable({
            start: function () {
               $(this).css('zIndex','100')
                if ($(this).parent().is('#end')) {
                    let droppedPuzzleOffset = getOffset($(this))
                    delete puzzlesOrder[droppedPuzzleOffset.top/25 + droppedPuzzleOffset.left/100]
                }
            },
            stop: function () {
                $(this).css('zIndex','99')
            }
        })
        $('#end').droppable({  
            accept: '.puzzle',
            over: function () {
                if(!timerStarted) {
                    timerStart()
                    timerStarted = true
                    $("#start-game").prop("disabled", true);
                    $("#check-result").prop("disabled", false);
                } 
            },
            drop: function(event, ui) {
                let droppedPuzzleOffset = getOffset($(ui.draggable))
                let leftIndex = Math.round(droppedPuzzleOffset.left/100)
                let topIndex = Math.round(droppedPuzzleOffset.top/100) 
                let puzzleIndex = topIndex*4 + leftIndex

                if(!puzzlesOrder[puzzleIndex]) {
                    puzzlesOrder[puzzleIndex] = ui.draggable.text()
                    ui.draggable.css('position', 'absolute')
                    ui.draggable.css('left', `${leftIndex*100}px`)
                    ui.draggable.css('top', `${topIndex*100}px`)
                    $('#end').append(ui.draggable)
                    console.log(puzzlesOrder)
                }
                else {
                    ui.draggable.css('left', '0')
                    ui.draggable.css('top', '0')
                }
            }
        })
    })

    $('#start-game').on('click', function() {
        timerStart()
        timerStarted = true
        $("#start-game").prop("disabled", true);
        $("#check-result").prop("disabled", false);
        
    }) 

    let check
    $('#check-result').on('click', function() {
        check = true
        if (puzzlesOrder.length != 16 || puzzlesOrder == undefined) {
            check = false;
        }
        else {
            for(let i = 0; i < 16; i++){
                if(puzzlesOrder[i] != i){
                    check = false;
                    break
                }
            }
        }    
        if (check) {
            $('.modal-body').html(`<h3>Congratulations!</h3>`)
            clearInterval(timerInterval);
        }
        else {
            modalInterval = setInterval(() => {
                $('.modal-body').html(`<p>You still have time, you sure?</p><br><h3>${mm}:${ss}</h3>`)
            })
            $('.modal-close').on('click', function() {clearInterval(modalInterval)})
        }
    })
})